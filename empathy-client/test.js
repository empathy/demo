#!/usr/bin/env node
let Exonum = require('exonum-client')
const explorerBasePath = 'http://192.168.43.29:8000/api/explorer/v1/transactions/'
const delegatesEndpoint = 'http://192.168.43.29:8000/api/services/empathy/v1/delegates'
const resolveEndpoint = 'http://192.168.43.29:8000/api/services/empathy/v1/delegates/resolve'

let ServiceDelegate = Exonum.newType({
  fields: [
    { name: 'issuer', type: Exonum.PublicKey },
    { name: 'uport_id', type: Exonum.String },
    { name: 'expiry', type: Exonum.String },
    { name: 'description', type: Exonum.String },
    { name: 'is_promise', type: Exonum.Uint8 },
    { name: 'resolution', type: Exonum.String }
  ]
})

let TxCreateServiceDelegate = Exonum.newMessage({
  protocol_version: 0,
  service_id: 1,
  message_id: 0,
  fields: [
    { name: 'issuer', type: Exonum.PublicKey },
    { name: 'uport_id', type: Exonum.String },
    { name: 'expiry', type: Exonum.String },
    { name: 'description', type: Exonum.String },
    { name: 'is_promise', type: Exonum.Uint8 }
  ]
})

let TxResolution = Exonum.newMessage({
  protocol_version: 0,
  service_id: 1,
  message_id: 1,
  fields: [
    { name: 'delegate', type: Exonum.PublicKey },
    { name: 'promise', type: Exonum.PublicKey },
    { name: 'seed', type: Exonum.Uint64 }
  ]
})

function createServiceDelegate(kp, uport_id, descr, is_promise) {
  const data = {
    issuer: kp.publicKey,
    uport_id: uport_id,
    expiry: 'never',
    description: descr,
    is_promise: is_promise
  }

  let signature = TxCreateServiceDelegate.sign(kp.secretKey, data)
  if (!Exonum.verifySignature(signature, kp.publicKey, data, TxCreateServiceDelegate))
      console.log("Can't verify signature")

  console.log("TxCreate: "+signature)
  Exonum.send(delegatesEndpoint, explorerBasePath, data, signature, TxCreateServiceDelegate)
    .then(tx => {
        console.log(tx)
    })
    .catch(err => {
        //console.log(err)
    })
}

function resolveServiceDelegate(delegateKP, promiseKP, relayerKP) {
  const data = {
    delegate: delegateKP.publicKey,
    promise: promiseKP.publicKey,
    seed: "0"
  }

  let signature = TxResolution.sign(relayerKP.secretKey, data)
  if (!Exonum.verifySignature(signature, relayerKP.publicKey, data, TxResolution))
      console.log("Can't verify signature")

  console.log("TxResolve: "+signature)
  Exonum.send(resolveEndpoint, explorerBasePath, data, signature, TxResolution)
    .then(tx => {
        console.log(tx)
    })
    .catch(err => {
        //console.log(err)
    })
}

const kpAlice = {
  publicKey: 'fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a',
  secretKey: '978e3321bd6331d56e5f4c2bdb95bf471e95a77a6839e68d4241e7b0932ebe2b' +
  'fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a'
}

//= Exonum.keyPair()
const kpBob = {
  publicKey: '83654edc87db5d68a8c141e03968e4debd07fdf2e898bfd9172aacac391e3e6c',
  secretKey: '6fc8a9b098f9ce31e800deac09feabf107631dbc0b11c0803272769aee2bd9ff' +
  '83654edc87db5d68a8c141e03968e4debd07fdf2e898bfd9172aacac391e3e6c'
}

const kpCarol = {
  publicKey: '2bee7759e078004d0eff4757754753ce84a9a5eaf63254472963a9345b951211',
  secretKey: 'ca5bc0674856f51caa3ca0185307599605c0de98f6b1c08fb5fa645479715228' +
  '2bee7759e078004d0eff4757754753ce84a9a5eaf63254472963a9345b951211'
}

createServiceDelegate(kpAlice, "0xf28c10ca43c1f68af4709f74e2934c100435806d", "print <3 Rust?", 0)
createServiceDelegate(kpBob, "0x3b898d8893a1176820dfa4d820c96f938c8ca39a", "print <3 Rust!", 1)

setTimeout(function(){
    resolveServiceDelegate(kpAlice, kpBob, kpCarol)
}, 3000);

