#!/bin/bash

promise_addr="83654edc87db5d68a8c141e03968e4debd07fdf2e898bfd9172aacac391e3e6c"
curl_output="curl_output.json"
known_issuers="known_issuers"

curl -H "Content-Type: application/json" -X POST -d @delegate-create-2.json http://192.168.43.29:8000/api/services/empathy/v1/delegates

while true
do
    curl -s http://192.168.43.29:8000/api/services/empathy/v1/delegates > ${curl_output}
    issuer_str=`cat ${curl_output} | grep "\"resolution\": \"${promise_addr}\"" -C 1 | grep issuer`
    issuer=`echo ${issuer_str} | cut -d' ' -f 2`

    touch $known_issuers
    if [ -n "${issuer}" ]; then
        if ! grep -Fq $issuer $known_issuers; then
            echo "<3 Rust";
            echo $issuer >> $known_issuers
        fi
    fi

    sleep 1
done
