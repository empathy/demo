#!/usr/bin/env node
let Exonum = require('exonum-client')
let request = require('request')

const explorerBasePath = 'http://192.168.43.29:8000/api/explorer/v1/transactions/'
const delegatesEndpoint = 'http://192.168.43.29:8000/api/services/empathy/v1/delegates'
const resolveEndpoint = 'http://192.168.43.29:8000/api/services/empathy/v1/delegates/resolve'

let TxResolution = Exonum.newMessage({
  protocol_version: 0,
  service_id: 1,
  message_id: 1,
  fields: [
    { name: 'delegate', type: Exonum.PublicKey },
    { name: 'promise', type: Exonum.PublicKey },
    { name: 'seed', type: Exonum.Uint64 }
  ]
})

function isPromise(delegate) {
    return delegate.is_promise
}

function isUnresolved(delegate) {
    return !isPromise(delegate) && delegate.resolution == ""
}

function resolveServiceDelegate(delegate_pk, promise_pk, relayerKeyPair) {
  const data = {
    delegate: delegate_pk,
    promise: promise_pk,
    seed: "0"
  }

  let signature = TxResolution.sign(relayerKeyPair.secretKey, data)
  if (!Exonum.verifySignature(signature, relayerKeyPair.publicKey, data, TxResolution))
      console.log("Can't verify signature")

  console.log("TxResolve: "+signature)
  Exonum.send(resolveEndpoint, explorerBasePath, data, signature, TxResolution)
    .then(tx => {
        console.log(tx)
    })
    .catch(err => {
        //console.log(err)
    })
}

//= Exonum.keyPair()
const kpCarol = {
  publicKey: '2bee7759e078004d0eff4757754753ce84a9a5eaf63254472963a9345b951211',
  secretKey: 'ca5bc0674856f51caa3ca0185307599605c0de98f6b1c08fb5fa645479715228' +
  '2bee7759e078004d0eff4757754753ce84a9a5eaf63254472963a9345b951211'
}

function seek_and_match() {
  request(delegatesEndpoint, function (error, response, body) {
    if(error) return
    var all_objects = JSON.parse(body) // let's just assume it's there :)
    var delegates = all_objects.filter(obj => isUnresolved(obj))
    var promises = all_objects.filter(obj => isPromise(obj))

    delegates.forEach(delegate => {
      let service = delegate.description.slice(0,-1)
      // behold! a really silly way to match delegates and promises...
      let matches = promises.filter(promise => promise.description.slice(0,-1) == service)
      if(matches.length > 0) {
        console.log("Going to resolve: " + delegate.issuer + " with " + matches[0].issuer)
        resolveServiceDelegate(delegate.issuer, matches[0].issuer, kpCarol)
      }
    })
  })
}

setInterval(seek_and_match, 1000)

