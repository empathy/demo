pragma solidity 0.4;

contract EmpathyRegistry {
  uint public version;
  address public previousRevision;
  mapping(address => mapping(address => bytes32)) public registry;

  function EmpathyRegistry(address _previousRevision) public {
    version = 1;
    previousRevision = _previousRevision;
  }

  event Set(
    address indexed device,
    address indexed owner,
    uint updatedAt);

  function set(address device, bytes32 ipfs_hash) public {
      Set(device, msg.sender, now);
      registry[device][msg.sender] = ipfs_hash;
  }

  function get(address device, address owner) public constant returns(bytes32){
      return registry[device][owner];
  }
}
