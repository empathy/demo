#!/bin/bash

# Submit delegates
curl -H "Content-Type: application/json" -X POST -d @scripts/delegate-create-1.json http://192.168.43.29:8000/api/services/empathy/v1/delegates
curl -H "Content-Type: application/json" -X POST -d @scripts/delegate-create-2.json http://192.168.43.29:8000/api/services/empathy/v1/delegates

# Resolve delegate
curl -H "Content-Type: application/json" -X POST -d @scripts/delegate-resolve.json http://192.168.43.29:8000/api/services/empathy/v1/delegates/resolve

# List delegates
sleep 3
curl -H "Content-Type: application/json" -X GET http://192.168.43.29:8000/api/services/empathy/v1/delegates
