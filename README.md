COMPONENT OVERVIEW
==================

./empathy-blockchain-exonum/ : blockchain logic based on exonum  
./empathy-client/ : client mockups to communicate with blockchain  
./empathy-registry/ : device ownership registry  
./empathy-uport/ : identity management extensions based on uport  
./scripts/ : test workflow example  

DEMO WORKFLOW
=============
> *Handy aliases:*  
> d: docker run -i --link ganache:node -v `pwd`:/usr/src/app -w /usr/src/app nenv /bin/sh -c "$@"  
> d-net: docker run -i --net=host -v `pwd`:/usr/src/app -w /usr/src/app nenv /bin/sh -c "$@"  

**Step 0. Build stuff**
```bash
docker build . -t nenv
cd empathy-client; npm install;
cd empathy-uport; npm install;
cd empathy-registry; yarn install;
```

**Step 1. Generate private/public keys and attest them on blockchain**
```bash
cd /Users/alex/web3/demo
d "cd empathy-uport; ./main.js create dev"
ls empathy-uport/uport-client
vim empathy-uport/uport-client/dev.json
```

**Step 2. Attest device with personal keys**
```bash
cd /Users/alex/web3/demo
d "cd empathy-uport; ./main.js identity dev"
d "cd empathy-uport; ./main.js certify tty"
```

**Step 3. Claim device ownership**
```bash
cd /Users/alex/web3/demo
d "cd empathy-registry; yarn compile; yarn deploy;"
> 0x3b2baf0cd4497d171fb2bc71226a006d53f9c890
d "cd empathy-uport; ./main.js own pi"
d "cd empathy-uport; ./main.js own tty"
```

> *Let’s check:*
> ``d "cd empathy-registry; truffle console --network rinkeby"``
> ```javascript
> EmpathyRegistry.deployed().then(inst => { emp = inst })
> emp.version.call()
> emp.get.call(<insert acc device>, "0xd13daa6b31fcb0ecc55d72cfc499b55c598eb436")
> ```

**Step 4. Launch blockchain**
```bash
cd /Users/alex/web3/demo/empathy-blockchain-exonum
cargo run --example demo
```

**Step 5. Launch provider**
> note: originally @ empathy-client/provider.sh

```bash
ssh vespa;
sh ./provider.sh
```

**Step 6. Launch relayer**
```bash
cd /Users/alex/web3/demo
d-net "cd empathy-client; ./relayer.js"
```

**Step 7. Launch consumer**
> note: originally @ empathy-client/client.sh

```bash
ssh pi@raspberrypi.local;
sh ./client.sh
```
