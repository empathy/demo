extern crate bodyparser;
#[macro_use]
extern crate exonum;
#[macro_use]
extern crate failure;
extern crate iron;
extern crate router;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

pub mod schema {
    use exonum::storage::{Fork, MapIndex, Snapshot};
    use exonum::crypto::PublicKey;

    encoding_struct! {
        struct ServiceDelegate {
            issuer: &PublicKey,
            uport_id: &str,
            expiry: &str,
            description: &str,
            is_promise: u8,
            resolution: &str,
        }
    }

    impl ServiceDelegate {
        /// Returns a copy of this delegate with the promise pub key as resolution.
        pub fn resolve(self, promise: &PublicKey) -> Self {
            Self::new(self.issuer(), self.uport_id(), self.expiry(), self.description(), self.is_promise(), &promise.to_string())
        }
    }

    /// Schema of the key-value storage
    pub struct EmpathySchema<T> {
        view: T,
    }

    impl<T: AsRef<Snapshot>> EmpathySchema<T> {
        pub fn new(view: T) -> Self {
            EmpathySchema { view }
        }

        pub fn delegates(&self) -> MapIndex<&Snapshot, PublicKey, ServiceDelegate> {
            MapIndex::new("empathy.delegates", self.view.as_ref())
        }

        pub fn delegate(&self, pub_key: &PublicKey) -> Option<ServiceDelegate> {
            self.delegates().get(pub_key)
        }
    }

    /// A mutable version of the schema with an additional method to persist into the storage.
    impl<'a> EmpathySchema<&'a mut Fork> {
        pub fn delegates_mut(&mut self) -> MapIndex<&mut Fork, PublicKey, ServiceDelegate> {
            MapIndex::new("empathy.delegates", &mut self.view)
        }
    }
}

pub mod transactions {
    use exonum::crypto::PublicKey;
    use service::SERVICE_ID;

    transactions! {
        pub EmpathyTransactions {
            const SERVICE_ID = SERVICE_ID;

            struct TxCreateServiceDelegate {
                issuer: &PublicKey,
                uport_id: &str,
                expiry: &str,
                description: &str,
                is_promise: u8,
            }

            struct TxResolution {
                delegate: &PublicKey,
                promise: &PublicKey,
                seed: u64, // replay protection
            }
        }
    }
}

pub mod errors {
    use exonum::blockchain::ExecutionError;

    #[derive(Debug, Fail)]
    #[repr(u8)]
    pub enum Error {
        /// Can be emitted by `TxCreateServiceDelegate`.
        #[fail(display = "Delegate already exists")]
        DelegateAlreadyExists = 0,

        /// Can be emitted by `TxResolution`.
        #[fail(display = "Delegate doesn't exist")]
        DelegateNotFound = 1,

        /// Can be emitted by `TxResolution`.
        #[fail(display = "Promise doesn't exist")]
        PromiseNotFound = 2,

        /// Can be emitted by `TxResolution`.
        #[fail(display = "Insufficient permissions")]
        InsufficientPermissions = 3,
    }

    impl From<Error> for ExecutionError {
        fn from(value: Error) -> ExecutionError {
            let description = format!("{}", value);
            ExecutionError::with_description(value as u8, description)
        }
    }
}

pub mod contracts {
    use exonum::blockchain::{ExecutionResult, Transaction};
    use exonum::{messages::Message, storage::Fork};

    use schema::{EmpathySchema, ServiceDelegate};
    use transactions::{TxCreateServiceDelegate, TxResolution};
    use errors::Error;

    impl Transaction for TxCreateServiceDelegate {
        fn verify(&self) -> bool {
            self.verify_signature(self.issuer())
        }

        fn execute(&self, view: &mut Fork) -> ExecutionResult {
            let mut schema = EmpathySchema::new(view);
            if schema.delegate(self.issuer()).is_none() {
                let delegate = ServiceDelegate::new(self.issuer(), self.uport_id(), self.expiry(), self.description(), self.is_promise(), &"");
                println!("Create the delegate: {:?}", delegate);
                schema.delegates_mut().put(self.issuer(), delegate);
                Ok(())
            } else {
                Err(Error::DelegateAlreadyExists)?
            }
        }
    }

    impl Transaction for TxResolution {
        fn verify(&self) -> bool {
            // TODO: verify relayer signature, verify permissions, protect against bad relayers
            (*self.delegate() != *self.promise())
        }

        fn execute(&self, view: &mut Fork) -> ExecutionResult {
            let mut schema = EmpathySchema::new(view);

            let delegate = match schema.delegate(self.delegate()) {
                Some(val) => val,
                None => Err(Error::DelegateNotFound)?,
            };

            // TODO: need to lookup the promise service offering
            /*let promise = match schema.delegates(self.promise()) {
                Some(val) => val,
                None => Err(Error::PromiseNotFound)?,
            };*/

            // if delegate.credentials() != promise.credentials() { // TODO: check permissions
                let resolved_delegate = delegate.resolve(self.promise());
                println!("Resolution for delegate: {:?}", resolved_delegate);
                let mut delegates = schema.delegates_mut();
                delegates.put(self.delegate(), resolved_delegate);
                Ok(())
            // } else {
            //     Err(Error::InsufficientPermissions)?
            // }
        }
    }
}

/// REST API
pub mod api {
    use exonum::blockchain::{Blockchain, Transaction};
    use exonum::encoding::serialize::FromHex;
    use exonum::node::{ApiSender, TransactionSend};
    use exonum::crypto::{Hash, PublicKey};
    use exonum::api::{Api, ApiError};
    use iron::prelude::*;
    use iron::{headers::ContentType, modifiers::Header, status::Status};
    use router::Router;

    use bodyparser;
    use serde_json;
    use schema::{EmpathySchema, ServiceDelegate};
    use transactions::EmpathyTransactions;

    /// Container for the service API.
    #[derive(Clone)]
    pub struct EmpathyApi {
        channel: ApiSender,
        blockchain: Blockchain,
    }

    impl EmpathyApi {
        pub fn new(channel: ApiSender, blockchain: Blockchain) -> EmpathyApi {
            EmpathyApi {
                channel,
                blockchain,
            }
        }
    }

    #[derive(Serialize, Deserialize)]
    pub struct TransactionResponse {
        pub tx_hash: Hash,
    }

    impl EmpathyApi {
        /// Endpoint for getting a single request.
        fn get_delegate(&self, req: &mut Request) -> IronResult<Response> {
            let path = req.url.path();
            let delegate_key = path.last().unwrap();
            let public_key = PublicKey::from_hex(delegate_key).map_err(|e| {
                IronError::new(
                    e,
                    (
                        Status::BadRequest,
                        Header(ContentType::json()),
                        "\"Invalid request param: `pub_key`\"",
                    ),
                )
            })?;

            let snapshot = self.blockchain.snapshot();
            let schema = EmpathySchema::new(snapshot);

            if let Some(delegate) = schema.delegate(&public_key) {
                self.ok_response(&serde_json::to_value(delegate).unwrap())
            } else {
                self.not_found_response(&serde_json::to_value("ServiceDelegate not found").unwrap())
            }
        }

        /// Endpoint for dumping all delegates from the storage.
        fn get_delegates(&self, _: &mut Request) -> IronResult<Response> {
            let snapshot = self.blockchain.snapshot();
            let schema = EmpathySchema::new(snapshot);
            let idx = schema.delegates();
            let delegates: Vec<ServiceDelegate> = idx.values().collect();

            self.ok_response(&serde_json::to_value(&delegates).unwrap())
        }

        /// Common processing for transaction-accepting endpoints.
        fn post_transaction(&self, req: &mut Request) -> IronResult<Response> {
            match req.get::<bodyparser::Struct<EmpathyTransactions>>() {
                Ok(Some(transaction)) => {
                    let transaction: Box<Transaction> = transaction.into();
                    let tx_hash = transaction.hash();
                    self.channel.send(transaction).map_err(ApiError::from)?;
                    let json = TransactionResponse { tx_hash };
                    self.ok_response(&serde_json::to_value(&json).unwrap())
                }
                Ok(None) => Err(ApiError::BadRequest("Empty request body".into()))?,
                Err(e) => Err(ApiError::BadRequest(e.to_string()))?,
            }
        }
    }

    /// GET `v1/delegate/:pub_key` ## Retrieve single delegate
    /// GET `v1/delegates` ## Dump all delegates
    /// POST `v1/delegates` ## Create new delegate [`TxCreateServiceDelegate`]
    /// POST `v1/delegates/resolve` ## Resolution of the delegate [`TxResolution`]

    impl Api for EmpathyApi {
        fn wire(&self, router: &mut Router) {
            let self_ = self.clone();
            let post_create_delegate = move |req: &mut Request| self_.post_transaction(req);
            let self_ = self.clone();
            let post_resolve = move |req: &mut Request| self_.post_transaction(req);
            let self_ = self.clone();
            let get_delegates = move |req: &mut Request| self_.get_delegates(req);
            let self_ = self.clone();
            let get_delegate = move |req: &mut Request| self_.get_delegate(req);

            /* TODO: add announce, wish, discover routes */

            router.post("/v1/delegates", post_create_delegate, "post_create_delegate");
            router.post("/v1/delegates/resolve", post_resolve, "post_resolve");
            router.get("/v1/delegates", get_delegates, "get_delegates");
            router.get("/v1/delegate/:pub_key", get_delegate, "get_delegate");
        }
    }
}

/// Service declaration
pub mod service {
    use exonum::blockchain::{ApiContext, Service, Transaction, TransactionSet};
    use exonum::{encoding, api::Api, crypto::Hash, messages::RawTransaction, storage::Snapshot};
    use iron::Handler;
    use router::Router;

    use transactions::EmpathyTransactions;
    use api::EmpathyApi;

    pub const SERVICE_ID: u16 = 1;

    pub struct EmpathyService;
    impl Service for EmpathyService {
        fn service_name(&self) -> &'static str {
            "empathy"
        }

        fn service_id(&self) -> u16 {
            SERVICE_ID
        }

        // Implement a method to deserialize transactions coming to the node.
        fn tx_from_raw(&self, raw: RawTransaction) -> Result<Box<Transaction>, encoding::Error> {
            let tx = EmpathyTransactions::tx_from_raw(raw)?;
            Ok(tx.into())
        }

        // Hashes for the service tables that will be included into the state hash.
        // TODO: Merkelized tables (https://exonum.com/doc/architecture/storage/#merklized-indices)
        fn state_hash(&self, _: &Snapshot) -> Vec<Hash> {
            vec![]
        }

        // Create a REST `Handler` to process web requests to the node.
        fn public_api_handler(&self, ctx: &ApiContext) -> Option<Box<Handler>> {
            let mut router = Router::new();
            let api = EmpathyApi::new(ctx.node_channel().clone(), ctx.blockchain().clone());
            api.wire(&mut router);
            Some(Box::new(router))
        }
    }
}
