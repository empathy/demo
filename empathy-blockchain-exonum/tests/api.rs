#[macro_use]
extern crate assert_matches;
extern crate exonum;
extern crate empathy_relayer_exonum;
extern crate exonum_testkit;
#[macro_use]
extern crate serde_json;

use exonum::api::ApiError;
use exonum::crypto::{self, CryptoHash, Hash, PublicKey, SecretKey};
use exonum_testkit::{ApiKind, TestKit, TestKitApi, TestKitBuilder};

use empathy_relayer_exonum::transactions::{TxCreateServiceDelegate, TxResolution};
use empathy_relayer_exonum::schema::ServiceDelegate;
use empathy_relayer_exonum::service::EmpathyService;

#[test]
fn test_create_delegate() {
    let (mut testkit, api) = create_testkit();
    // Create and send a transaction via API
    let (tx, _) = api.create_delegate("print <3 Rust?");
    testkit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    // Check that the user indeed is persisted by the service.
    let delegate = api.get_delegate(tx.issuer());
    assert_eq!(delegate.issuer(), tx.issuer());
    assert_eq!(delegate.description(), tx.description());
    assert_eq!(delegate.is_promise(), tx.is_promise());
    assert_eq!(delegate.resolution(), "");
}

/// Check that the resolve transaction works as intended.
#[test]
fn test_resolve() {
    // Create 2 delegates.
    let (mut testkit, api) = create_testkit();
    let (tx_alice, key_alice) = api.create_delegate("print <3 Rust?");
    let (tx_bob, _) = api.create_delegate("print <3 Rust!");
    testkit.create_block();
    api.assert_tx_status(&tx_alice.hash(), &json!({ "type": "success" }));
    api.assert_tx_status(&tx_bob.hash(), &json!({ "type": "success" }));

    let tx = TxResolution::new(
        tx_alice.issuer(),
        tx_bob.issuer(),
        0,  // seed
        &key_alice,
    );
    api.resolve(&tx);
    testkit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    let delegate = api.get_delegate(tx_alice.issuer());
    assert_eq!(delegate.resolution(), &tx_bob.issuer().to_string());
}

/// Check that a resolve of a non-existing delegate fails as expected.
#[test]
fn test_resolve_of_nonexisting_delegate() {
    let (mut testkit, api) = create_testkit();

    let (tx_alice, key_alice) = api.create_delegate("print <3 Rust?");
    let (tx_bob, _) = api.create_delegate("print <3 Rust!");
    // Do not commit Alice's transaction, so Alice's delegate does not exist
    // when a resolve occurs.
    testkit.create_block_with_tx_hashes(&[tx_bob.hash()]);

    api.assert_no_delegate(tx_alice.issuer());
    let _delegate = api.get_delegate(tx_bob.issuer());
    //assert_eq!(delegate exists)

    let tx = TxResolution::new(
        tx_alice.issuer(),
        tx_bob.issuer(),
        0,  // seed
        &key_alice,
    );
    api.resolve(&tx);
    testkit.create_block_with_tx_hashes(&[tx.hash()]);
    api.assert_tx_status(
        &tx.hash(),
        &json!({ "type": "error", "code": 1, "description": "Delegate doesn't exist" }),
    );
}

#[test]
fn test_malformed_delegate_request() {
    let (_testkit, api) = create_testkit();

    let info = api.inner
        .get_err(ApiKind::Service("empathy"), "v1/delegate/c0ffee");
    assert_matches!(
        info,
        ApiError::BadRequest(ref body) if body.starts_with("Invalid request param")
    );
}

#[test]
fn test_unknown_delegate_request() {
    let (_testkit, api) = create_testkit();

    // Transaction is sent by API, but isn't committed.
    let (tx, _) = api.create_delegate("print <3 Rust");

    let info = api.inner.get_err(
        ApiKind::Service("empathy"),
        &format!("v1/delegate/{}", tx.issuer().to_string()),
    );
    assert_matches!(
        info,
        ApiError::NotFound(ref body) if body == "ServiceDelegate not found"
    );
}

/// Wrapper for the empathy service API allowing to easily use it
/// (compared to `TestKitApi` calls).
struct EmpathyApi {
    pub inner: TestKitApi,
}

impl EmpathyApi {
    /// Generates a delegate creation transaction with a random key pair, sends it over HTTP,
    /// and checks the synchronous result (i.e., the hash of the transaction returned
    /// within the response).
    /// Note that the transaction is not immediately added to the blockchain, but rather is put
    /// to the pool of unconfirmed transactions.
    fn create_delegate(&self, description: &str) -> (TxCreateServiceDelegate, SecretKey) {
        let (pubkey, key) = crypto::gen_keypair();
        // Create a pre-signed transaction
        let tx = TxCreateServiceDelegate::new(&pubkey, "mnid", "never", description, 1u8, &key); // is_promise: 1

        let tx_info: serde_json::Value = self.inner.post(ApiKind::Service("empathy"), "v1/delegates", &tx);
        assert_eq!(tx_info, json!({ "tx_hash": tx.hash() }));
        (tx, key)
    }

    /// Sends a resolve transaction over HTTP and checks the synchronous result.
    fn resolve(&self, tx: &TxResolution) {
        let tx_info: serde_json::Value = self.inner.post(
            ApiKind::Service("empathy"),
            "v1/delegates/resolve",
            tx,
        );
        assert_eq!(tx_info, json!({ "tx_hash": tx.hash() }));
    }

    /// Gets the state of a particular delegate using an HTTP request.
    fn get_delegate(&self, pubkey: &PublicKey) -> ServiceDelegate {
        self.inner.get(
            ApiKind::Service("empathy"),
            &format!("v1/delegate/{}", pubkey.to_string()),
        )
    }

    /// Asserts that a delegate with the specified public key is not known to the blockchain.
    fn assert_no_delegate(&self, pubkey: &PublicKey) {
        let err = self.inner.get_err(
            ApiKind::Service("empathy"),
            &format!("v1/delegate/{}", pubkey.to_string()),
        );

        assert_matches!(
            err,
            ApiError::NotFound(ref body) if body == "ServiceDelegate not found"
        );
    }

    /// Asserts that the transaction with the given hash has a specified status.
    fn assert_tx_status(&self, tx_hash: &Hash, expected_status: &serde_json::Value) {
        let info: serde_json::Value = self.inner.get(
            ApiKind::Explorer,
            &format!("v1/transactions/{}", tx_hash.to_string()),
        );
        if let serde_json::Value::Object(mut info) = info {
            let tx_status = info.remove("status").unwrap();
            assert_eq!(tx_status, *expected_status);
        } else {
            panic!("Invalid transaction info format, object expected");
        }
    }
}

/// Creates a testkit together with the API wrapper defined above.
fn create_testkit() -> (TestKit, EmpathyApi) {
    let testkit = TestKitBuilder::validator()
        .with_service(EmpathyService)
        .create();
    let api = EmpathyApi {
        inner: testkit.api(),
    };
    (testkit, api)
}
