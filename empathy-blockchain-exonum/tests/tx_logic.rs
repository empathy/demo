extern crate exonum;
extern crate empathy_relayer_exonum;
#[macro_use]
extern crate exonum_testkit;
extern crate rand;

use exonum::crypto::{self, PublicKey, SecretKey};
use exonum_testkit::{TestKit, TestKitBuilder};

use empathy_relayer_exonum::schema::{EmpathySchema, ServiceDelegate};
use empathy_relayer_exonum::transactions::{TxCreateServiceDelegate, TxResolution};
use empathy_relayer_exonum::service::EmpathyService;

#[test]
fn test_create_delegate() {
    let mut testkit = init_testkit();
    let (tx, _) = create_delegate(&mut testkit, "print <3 Rust");

    // Check that the user indeed is persisted by the service
    let delegate = get_delegate(&testkit, tx.issuer());
    assert_eq!(delegate.issuer(), tx.issuer());
    assert_eq!(delegate.description(), "print <3 Rust");
    assert_eq!(delegate.is_promise(), 1u8);
    assert_eq!(delegate.uport_id(), "mnid");
    assert_eq!(delegate.expiry(), "never");
}

#[test]
fn test_resolution() {
    let mut testkit = init_testkit();
    let (alice_pubkey, alice_key) = crypto::gen_keypair();
    let (bob_pubkey, bob_key) = crypto::gen_keypair();
    testkit.create_block_with_transactions(txvec![
        TxCreateServiceDelegate::new(&alice_pubkey, "mnid", "never", "print <3 Rust?", 1u8, &alice_key),
        TxCreateServiceDelegate::new(&bob_pubkey, "mnid", "never", "print <3 Rust!", 1u8, &bob_key),
        TxResolution::new(
            &alice_pubkey,
            &bob_pubkey,
            /* seed */ 0,
            &alice_key
        ),
    ]);

    let alice_delegate = get_delegate(&testkit, &alice_pubkey);
    assert_eq!(alice_delegate.resolution(), &bob_pubkey.to_string());
}

#[test]
fn test_resolution_of_nonexisting_delegate() {
    let mut testkit = init_testkit();
    let (alice_pubkey, alice_key) = crypto::gen_keypair();
    let (bob_pubkey, bob_key) = crypto::gen_keypair();
    testkit.create_block_with_transactions(txvec![
        TxCreateServiceDelegate::new(&bob_pubkey, "mnid", "never", "print <3 Rust!", 1u8, &bob_key),
        TxResolution::new(
            &alice_pubkey,
            &bob_pubkey,
            /* seed */ 0,
            &alice_key
        ),
    ]);

    assert!(try_get_delegate(&testkit, &alice_pubkey).is_none());
}

#[test]
fn test_resolution_with_nonexisting_promise() {
    let mut testkit = init_testkit();
    let (alice_pubkey, alice_key) = crypto::gen_keypair();
    let (bob_pubkey, bob_key) = crypto::gen_keypair();
    testkit.create_block_with_transactions(txvec![
        TxCreateServiceDelegate::new(&alice_pubkey, "mnid", "never", "print <3 Rust?", 1u8, &alice_key),
        TxResolution::new(
            &alice_pubkey,
            &bob_pubkey,
            /* seed */ 0,
            &alice_key
        ),
        // Although Bob's delegate is created, this occurs after the resolution is executed.
        TxCreateServiceDelegate::new(&bob_pubkey, "mnid", "never", "print <3 Rust!", 1u8, &bob_key),
    ]);

    let alice_delegate = get_delegate(&testkit, &alice_pubkey);
    assert_eq!(alice_delegate.resolution(), &bob_pubkey.to_string()); // THIS SHOULD BE THIS INSTEAD! --> alice_delegate.issuer()); // TODO: check against NULL
}

fn init_testkit() -> TestKit {
    TestKitBuilder::validator()
        .with_service(EmpathyService)
        .create()
}

fn create_delegate(testkit: &mut TestKit, description: &str) -> (TxCreateServiceDelegate, SecretKey) {
    let (pubkey, key) = crypto::gen_keypair();
    let tx = TxCreateServiceDelegate::new(&pubkey, "mnid", "never", description, 1u8, &key);
    testkit.create_block_with_transaction(tx.clone());
    (tx, key)
}

fn try_get_delegate(testkit: &TestKit, pubkey: &PublicKey) -> Option<ServiceDelegate> {
    let snapshot = testkit.snapshot();
    EmpathySchema::new(&snapshot).delegate(pubkey)
}

fn get_delegate(testkit: &TestKit, pubkey: &PublicKey) -> ServiceDelegate {
    try_get_delegate(testkit, pubkey).expect("No delegate persisted")
}
